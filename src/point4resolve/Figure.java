
package point4resolve;

import interfaces.IFigure;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.LinkedList;


public class Figure implements IFigure{
    private LinkedList<Rectangle> figure;
    private ArrayList<Point> fgr_xy;

    public Figure(){
        figure = new LinkedList<>();
        fgr_xy = new ArrayList<>();
    }
    
    public LinkedList<Rectangle> getFigure(){
        return figure;
    }
    
    public void setRect(int y,int x,Point myUnit){
        figure.add(new Rectangle());
        figure.getLast().y = myUnit.y*y;
        figure.getLast().x = myUnit.x*x;
        figure.getLast().height = myUnit.y;
        figure.getLast().width = myUnit.x;        
    }  
    
    public void setP(Point myUnit){
        createP();
        
        for(int i=0;i<fgr_xy.size();i++){
            figure.get(i).y = myUnit.y*fgr_xy.get(i).y;
            figure.get(i).x = myUnit.x*fgr_xy.get(i).x;
            figure.get(i).height = myUnit.y;
            figure.get(i).width = myUnit.x; 
        }
        
    } 
    
    public void fIndPoint(LinkedList<Point> p,Point myUnit){
       
        for(int i=0;i<figure.size();i++)
        {
            figure.get(i).y = p.get(i).y*myUnit.y;
            figure.get(i).x = p.get(i).x*myUnit.x;
        }
    }
    
    public void rmRect(int l,int row){
        int y = row * l;
        int s = figure.size();
        for(int i=0;i<s;i++)
            for(int j=0;j<figure.size();j++)
                if(figure.get(j).y == y){
                    figure.remove(j);
                    break;
                }
        
        ReEnable(l,row);
    }
    
    private void ReEnable(int l,int row){ 
        int y = row * l;
        for(int i=0;i<figure.size();i++)
            if(figure.get(i).y < y)
                figure.get(i).y += row;        
    }
    
    private void createP(){
        if(fgr_xy.size()>0)
            fgr_xy.clear();
        
        for(int i=0;i<figure.size();i++)
            fgr_xy.add(new Point(figure.get(i).x/figure.get(i).width,figure.get(i).y/figure.get(i).height));        
    } 
}

