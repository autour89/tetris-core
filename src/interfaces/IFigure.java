
package interfaces;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.LinkedList;

public interface IFigure {
    LinkedList<Rectangle> getFigure();
    
    void setRect(int y,int x,Point myUnit);
    
    void setP(Point myUnit);
    
    void fIndPoint(LinkedList<Point> p,Point myUnit);
    
    void rmRect(int l,int row);
}
