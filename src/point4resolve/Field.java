
package point4resolve;

import interfaces.IField;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Collections;
import java.util.LinkedList;


// 0 - free place
// 1 - figure move
// 2 - level set
// 3 - claster add
// 4 - wall edge 


public class Field implements IField{
    private int f_H = 18;
    private int f_W = 16;
    private int[][] field;
    private LinkedList<Point> arrayPoints;
    private LinkedList<Integer> levelY;
    
    private int level = f_H-2;
    private Point center;
    
    
   public Field(){
        field = new int[f_H][f_W];
         
        for(int i=0;i<f_H;i++)
            for(int j=0;j<f_W;j++)
                if(i==0 || (j == 0 || j == f_W-1) )
                    field[i][j] = 4;
                else if(i == f_H-1)
                    field[i][j] = 3;
        
        arrayPoints = new LinkedList<>();
        levelY = new LinkedList<>();
        center = new Point(8,2);
    }
    
    public void setUnit(int row,int clmn){
        field[row][clmn] = 1;
    }
    
    public LinkedList<Point> get_Point(){
       arrayPoints.clear();

         for(int i=center.y-1;i<(center.y-1)+3;i++)
             for(int j=center.x-1;j<(center.x-1)+3;j++)
                 if(field[i][j]==1)
                     arrayPoints.add(new Point(j,i));
        return arrayPoints;
    }   
    
    public void moveOne(int flag){

        switch(flag){
            case 0:
                for(int i=center.y-1;i<(center.y-1)+3;i++)
                    for(int j=center.x+1;j>(center.x+1)-3;j--)
                        if(field[i][j]==1){
                            field[i][j]=0;
                            field[i][j+1]=1;
                        }
                 center.x++;     
                break;
            case 1:
                for(int i=center.y-1;i<(center.y-1)+3;i++)
                    for(int j=center.x-1;j<(center.x-1)+3;j++)
                        if(field[i][j]==1){
                            field[i][j]=0;
                            field[i][j-1]=1;
                        }
                        center.x--;
                break;
            case 3:
                for(int i=center.y+1;i>(center.y+1)-3;i--)
                    for(int j=center.x-1;j<(center.x-1)+3;j++)
                        if(field[i][j]==1){
                            field[i][j]=0;
                            field[i+1][j]=1;
                        }
                        center.y++;
                break;
        }
    }
   
    public void fIndNext(){
       if(field[center.y-1][center.x] ==0){            
            sideNext(0);
            sideNext(3);
            sideNext(1);
        }
        else if(field[center.y+1][center.x] ==0 ){                     
           sideNext(1);
           sideNext(2);
           sideNext(0);
        }
        else if(field[center.y][center.x-1] ==0){          
           sideNext(3);
           sideNext(1);
           sideNext(2);
        }       
        else if(field[center.y][center.x+1] ==0 ){
           sideNext(2);
           sideNext(0);
           sideNext(3);           
        }        
        setAngle();
    }
    
    private void sideNext(int flag){
        switch(flag){
            case 0:
                if(field[center.y][center.x-1]==1){
                    field[center.y][center.x-1]=0;
                    field[center.y-1][center.x]=1;
                }
                break;
            case 1:
                if(field[center.y][center.x+1]==1){
                    field[center.y][center.x+1]=0;
                    field[center.y+1][center.x]=1;
                }
                break;
            case 2:
                if(field[center.y-1][center.x]==1){
                    field[center.y-1][center.x]=0;
                    field[center.y][center.x+1]=1;
                }
                break;
            case 3:
                if(field[center.y+1][center.x]==1){
                    field[center.y+1][center.x]=0;
                    field[center.y][center.x-1]=1;
                }
                break;
        }
    }
    
    private void setAngle(){
        if(field[center.y+1][center.x+1]==1){
            field[center.y+1][center.x+1]=0;
            field[center.y+1][center.x-1]=1;
        }
        else if(field[center.y-1][center.x+1]==1){
            field[center.y-1][center.x+1]=0;
                field[center.y+1][center.x+1]=1;
        }
        else if(field[center.y-1][center.x-1]==1){
            field[center.y-1][center.x-1]=0;
            field[center.y-1][center.x+1]=1;
        }
        else if(field[center.y+1][center.x-1]==1){
            field[center.y+1][center.x-1]=0;
            field[center.y-1][center.x-1]=1;
        }
    }
    
    public Boolean checkMove(int flag){
        int countOfok = 0;
        get_Point();
        switch(flag){
            case 0:
                for(int i=0;i<arrayPoints.size();i++)
                    if(field[arrayPoints.get(i).y][arrayPoints.get(i).x+1]==0 || field[arrayPoints.get(i).y][arrayPoints.get(i).x+1]==1 )
                        countOfok++;
                break;
            case 1:
                for(int i=0;i<arrayPoints.size();i++)
                    if(field[arrayPoints.get(i).y][arrayPoints.get(i).x-1]==0 || field[arrayPoints.get(i).y][arrayPoints.get(i).x-1]==1 )
                        countOfok++;
                break;
            case 2:
                for(int i=0;i<arrayPoints.size();i++)
                    if(field[arrayPoints.get(i).y-1][arrayPoints.get(i).x]==0 || field[arrayPoints.get(i).y-1][arrayPoints.get(i).x]==1 )
                        countOfok++;
                break;
            case 3:
                for(int i=0;i<arrayPoints.size();i++)
                    if(field[arrayPoints.get(i).y+1][arrayPoints.get(i).x]==0 || field[arrayPoints.get(i).y+1][arrayPoints.get(i).x]==1 )
                        countOfok++;
                break;
        }
        return countOfok==arrayPoints.size();
    }
    
    
    public Boolean isNext(){
        get_Point();
        
        int max = getMaxY();
        
        for(int i=0;i<arrayPoints.size();i++)
            if(field[arrayPoints.get(i).y+1][arrayPoints.get(i).x]==3){
                level = max;
                
                for(int j=0;j<arrayPoints.size();j++)
                    field[arrayPoints.get(j).y][arrayPoints.get(j).x]=3;
                center.x = 8;
                center.y = 2;
                return false;
            }
            
        return true;
    }
    
    
    public int getMaxY()
    {
        levelY.clear();
        
        for(int i=0;i<arrayPoints.size();i++)
        {
           levelY.add(arrayPoints.get(i).y);           
        }
        
        int max = Collections.max(levelY);
        
        return max;
    }
    
    public Boolean isLevel(){
        int cnt = 0;
        for(int i=0;i<f_W;i++)
            if( field[level][i]==3)
              cnt++;
        return cnt==(f_W-2);
    }
        
    public int getLevel(){
        return level;
    }
    
    public void rmLevel(){
        for(int i=1;i<f_W-1;i++)
            field[level][i]=0;
        
//       field[level][0]=4;    
//    if(level++ !=f_H-2)
//       level ++;
    
       reEnable();
    }
    
    private void reEnable(){
        for(int i=level;i>1;i--)//f_H-3 level
           for(int j=1;j<f_W-1;j++)
               if(field[i][j]==3){
                   field[i+1][j]=3;
                   field[i][j]=0;
               }
    }
       
    public void reset(){
        for(int i=1;i<f_H-1;i++)
            for(int j=1;j<f_W-1;j++)
                if(field[i][j] == 3 || field[i][j] ==1 )
                    field[i][j] = 0;
        
        center.x = 8;
        center.y = 2;
    }

}

