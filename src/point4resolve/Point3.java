
package point4resolve;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.Timer;


public class Point3 extends MainPanel{
    private Game gm;
    private ActionPerform actp;
    
    private final JFrame mainFrame;
    private final MenuBar menuBar;
    private final Menu menu;
    private final MenuItem newGame;
    private final MenuItem settingsMenu;
    private final MenuItem exitMenu;
    
    public Point3(){        
        
        mainFrame = new JFrame();
        mainFrame.add(this);
        
        gm = new Game(this);
        actp = new ActionPerform(gm,mainFrame);
        this.addKeyListener(new KeyChecker(gm,actp));
        
        menuBar = new MenuBar();
        menu = new Menu();
        menu.setLabel("File");
        newGame = new MenuItem("New Game");
        newGame.setActionCommand("3");
        newGame.addActionListener(actp);
                
        settingsMenu = new MenuItem("Settings");
        settingsMenu.setActionCommand("4");
        settingsMenu.addActionListener(actp);
        
        exitMenu = new MenuItem("Exit");
        exitMenu.setActionCommand("5");
        exitMenu.addActionListener(actp);

        menu.add(newGame);
        menu.add(settingsMenu);
        menu.add(exitMenu);
        menuBar.add(menu);
        
        
        mainFrame.setSize(900, 650);
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        

    }
    
    public static void main(String[] args) {
       new Point3();
    }
    
}


class KeyChecker extends KeyAdapter  {
    private Game gm;   
    private ActionPerform actions;
    
    KeyChecker(Game g,ActionPerform actn){
        gm = g;
        actions = actn;
    }
        
    public void keyPressed(KeyEvent event) {
        char ch = event.getKeyChar();
        
        if(ch==' ')
        {
            actions.setPauseFlag();
        }
        else
            gm.MoveEvent(ch);    
     }
    
}

class ActionPerform implements ActionListener{
    private Timer t;
    private Game gm;
    private Button pauseBtn;
    private JFrame mainFrame;
    private  boolean  isStop = false;
    
    ActionPerform(Game g,JFrame frame){
        gm = g;
        mainFrame = frame;
        t = new Timer(200,this);
        t.setActionCommand("2");
        t.start();
        pauseBtn = gm.getPanel().getPauseBtn();
        pauseBtn.addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e) {
        int action = Integer.parseInt(e.getActionCommand());
        
        switch(action)
        {
            case 1:
                setPauseFlag();
                break;
            case 2:
                gm.MoveEvent('s');
                break;
            case 3:
                gm.newGame();
                break;
            case 4:
                showSettingDialog();
                break;
            case 5:
                System.exit(0);
                break;
        }
    }
    
    private void showSettingDialog()
    {
        JDialog dialog = new JDialog(mainFrame);
                dialog.setModal(true);
                dialog.pack();
                dialog.setSize(400, 300);
                dialog.setLocationRelativeTo(mainFrame);
                dialog.setVisible(true);
    }
    
    private void  setLabelPause()
    {
        if(isStop){
            t.stop();
            pauseBtn.setLabel("Start");
        }
        else{
            pauseBtn.setLabel("Pause");
            t.restart();
        }
    }
    
    public void  setPauseFlag()
    {
        isStop = !isStop;
        setLabelPause();
    }

}