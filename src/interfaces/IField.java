
package interfaces;
import java.awt.Point;
import java.util.LinkedList;

public interface IField {
    void setUnit(int row,int clmn);
    LinkedList<Point> get_Point();
    void moveOne(int flag);
    void fIndNext();
    /**
     * Check is figure can move to direction or not
     * @return  true or false
     */
    Boolean checkMove(int flag);
    /**
     * Check is figure can move next or not
     * @return  true or false
     */
    Boolean isNext();
    /**
     * Check is current line level is fulfilled or not
     * @return  true or false
     */
    Boolean isLevel();
    int getMaxY();
    int getLevel();
    void rmLevel();
    void reset();
}
