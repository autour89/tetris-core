
package point4resolve;

import interfaces.IField;
import interfaces.IGame;
import java.util.Random;


public class Game implements IGame{
    private MainPanel mpanel;
    private IField field;
    private Random random;
    private int xFigure;
    
    public Game(MainPanel m){
        mpanel = m;
        random = new Random();
        field = new Field();
        
        createOne();
    }
    
    public MainPanel getPanel()
    {
        return mpanel;
    }
    public void MoveEvent(char ch){
        switch (ch) {
            case 'd':
                if(field.checkMove(0))
                    moveFigure(0);
                break;
            case 'a':
                if(field.checkMove(1))
                    moveFigure(1);
                break;
            case 'w':
                field.fIndNext();
                mpanel.getPanel().getFigure().getLast().fIndPoint(field.get_Point(), mpanel.getPanel().getUnitSz());
                break;
           case 's':
               if(field.checkMove(3))
                   moveFigure(3);
                break;            
        }
        sendMessage();
    }  
    
    private void sendMessage(){
        if(!field.isNext()){            
            if(field.isLevel())
                rmFigure();
            
            createOne();
        }
        mpanel.getPanel().repaint();
    }
    
    private void rmFigure(){

        for(int i=0;i<mpanel.getPanel().getFigure().size();i++)
            mpanel.getPanel().getFigure().get(i).rmRect(field.getLevel(),mpanel.getPanel().getUnitSz().y);
        
        field.rmLevel();
        
        if(field.isLevel())
            rmFigure();
        
        mpanel.addChange();

    }
     
    private void createOne(){
        xFigure = random.nextInt(7);
        mpanel.getPanel().getFigure().addLast(new Figure());        
        creteFigure(xFigure);
    }
      
    private void creteFigure(int type){
       switch(type){
           case 0:
                for(int i=7;i<10;i++){
                    mpanel.getPanel().getFigure().getLast().setRect(2,i,mpanel.getPanel().getUnitSz());
                    field.setUnit(2,i);
                }
               break;
            case 1:
                for(int i=1;i<4;i++){
                    mpanel.getPanel().getFigure().getLast().setRect(i,8,mpanel.getPanel().getUnitSz());
                    field.setUnit(i,8);
                }
               break;
            case 2:
                for(int i=8;i<10;i++)
                    for(int j=2;j<4;j++){
                        mpanel.getPanel().getFigure().getLast().setRect(j,i,mpanel.getPanel().getUnitSz());
                        field.setUnit(j,i);
                    }                       
               break;
            case 3:
                for(int i=7;i<10;i++)
                    if(i!=7){
                        mpanel.getPanel().getFigure().getLast().setRect(2,i,mpanel.getPanel().getUnitSz());
                        field.setUnit(2,i);
                    }
                    else
                        for(int j=1;j<3;j++){
                            mpanel.getPanel().getFigure().getLast().setRect(j,i,mpanel.getPanel().getUnitSz());
                            field.setUnit(j,i);
                        }
               break;
            case 4:
                for(int i=7;i<10;i++)
                    if(i!=8)
                    {
                        mpanel.getPanel().getFigure().getLast().setRect(2,i,mpanel.getPanel().getUnitSz());
                        field.setUnit(2,i);
                    }
                    else
                        for(int j=1;j<3;j++){
                            mpanel.getPanel().getFigure().getLast().setRect(j,i,mpanel.getPanel().getUnitSz());
                            field.setUnit(j,i);
                        }
               break;
            case 5:
                for(int i=7;i<10;i++)
                    if(i!=9){
                        mpanel.getPanel().getFigure().getLast().setRect(2,i,mpanel.getPanel().getUnitSz());
                        field.setUnit(2,i);
                    }
                    else
                        for(int j=1;j<3;j++){
                            mpanel.getPanel().getFigure().getLast().setRect(j,i,mpanel.getPanel().getUnitSz());
                            field.setUnit(j,i);
                        }                
               break;
            case 6:
                for(int i=7;i<9;i++){
                    mpanel.getPanel().getFigure().getLast().setRect(2,i,mpanel.getPanel().getUnitSz());
                    field.setUnit(2,i);
                }
               break;
       }
    }
    
    private void moveFigure(int flag){
        switch(flag){
            case 0:
                for(int i=0;i<field.get_Point().size();i++)//mp.getPn().getFg().getLast().getFigure().size()
                    mpanel.getPanel().getFigure().getLast().getFigure().get(i).x +=mpanel.getPanel().getUnitSz().x;                
                field.moveOne(0);
                break;
            case 1:
                for(int i=0;i<field.get_Point().size();i++)
                    mpanel.getPanel().getFigure().getLast().getFigure().get(i).x -=mpanel.getPanel().getUnitSz().x;
                field.moveOne(1);
                break;
            case 3:
                for(int i=0;i<field.get_Point().size();i++)
                    mpanel.getPanel().getFigure().getLast().getFigure().get(i).y +=mpanel.getPanel().getUnitSz().y;
                field.moveOne(3);
                break;
        }
    }
    
    public void newGame(){
        mpanel.getPanel().getFigure().clear();
        field.reset();
        createOne();
        mpanel.getPanel().repaint();
        
        mpanel.resetCount();
    }
}
