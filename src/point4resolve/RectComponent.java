
package point4resolve;

import interfaces.IFigure;
import java.awt.*;
import java.util.LinkedList;
import javax.swing.JComponent;

public class RectComponent extends JComponent {
    private Graphics2D g2d;
    private Rectangle r;
    private LinkedList<IFigure> fgr;
    private Point UnitS;
    
    public RectComponent(int w,int h){
        fgr = new LinkedList<>();
        UnitS = new Point((w-10)/16,(h-10)/18);
        r = new Rectangle();
        r.x = 10;
        r.y = 10;
    }
    
   public void paintComponent(Graphics g){
        
        super.paintComponent(g);
        g2d = (Graphics2D)g;
        
        drawField(g2d);
        drawFigures(g2d);  
        
        g2d.setStroke(new BasicStroke(3));
        g2d.draw(r);
    }
    
    public void newSz(int w,int h){
       if((w>200 && h>200) || h>200){
           r.width = w-40;
           r.height = h-40;
           UnitS.x = (w-10)/16;//r.width/16;
           UnitS.y = (h-10)/18; //r.height/18;
           
           for(int i=0;i<fgr.size();i++)
               fgr.get(i).setP(UnitS);
           
       }
       
       
       
   }
   
   private void drawField(Graphics2D g2){
       g2.setColor(Color.ORANGE);
       g2.setStroke(new BasicStroke(1));
        
       for(int i=0;i<fgr.getLast().getFigure().size();i++)
                fgr.getLast().getFigure().get(i).setLocation(fgr.getLast().getFigure().get(i).x,fgr.getLast().getFigure().get(i).y);
    }
    
   private void drawFigures(Graphics2D g2){
        for(int i=0;i<fgr.size();i++)
                for(int j=0;j<fgr.get(i).getFigure().size();j++){
                    g2.fill(fgr.get(i).getFigure().get(j));
                    g2.setColor(Color.BLACK);
                    g2.draw(fgr.get(i).getFigure().get(j));    
                    g2.setColor(Color.ORANGE);                    
                }
    }
   
   public LinkedList<IFigure> getFigure(){
        return fgr;
    }
   
   public Point getUnitSz(){
        return UnitS;
    }
    
}

