
package point4resolve;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.JPanel;

public class MainPanel extends JPanel{
    
    private RectComponent rc;
    private int count = 0;
    
    private JPanel mainPanel;
    private final JPanel rightPanel;
    private final Border lineBorder;
    private final JScrollPane scrollp;
    private final JLabel jlabel;
    private final Button pauseBtn;
    
    
    MainPanel(){
        
        lineBorder = BorderFactory.createLineBorder(Color.BLACK,2);
        
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        mainPanel = new JPanel(new GridLayout());
        jlabel = new JLabel("");
        //jlabel.setFont(new Font("Verdana",1,20));
        mainPanel.setSize(500, 700);
        mainPanel.setPreferredSize(new Dimension(mainPanel.getWidth(),0));
        mainPanel.setVisible(true);
        rc = new RectComponent(mainPanel.getWidth(), mainPanel.getHeight());
        mainPanel.add(rc);
        
        pauseBtn = new Button("Pause");
        pauseBtn.setActionCommand("1");
        pauseBtn.setFocusable(false);
        rightPanel = new JPanel();//GridLayout(20, 1, 0, 5));//setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        rightPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
//        rightPanel = new JPanel(new GridLayout(20, 1, 0, 5));
        rightPanel.setPreferredSize(new Dimension(200,0));
        rightPanel.setVisible(true);
        rightPanel.setBorder(lineBorder);
        rightPanel.add(jlabel);
        rightPanel.add(pauseBtn);
        
        scrollp = new JScrollPane(rightPanel);
        this.add(mainPanel);
        this.add(rightPanel);
        this.setFocusable(true);
        this.requestFocusInWindow();
        
        mainPanel.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                rc.newSz(mainPanel.getWidth(), mainPanel.getHeight());
            }
        });
        
        
        
    }
    
    
    public RectComponent getPanel(){
        return rc;
    }
    
    public Button getPauseBtn(){
        return pauseBtn;
    }
    
    
    public void addChange(){
        count +=10;
        jlabel.setText("" + count);
    }
    
    public void resetCount(){
        count = 0;
        jlabel.setText("" + 0);
    }
    
}

